#!/usr/bin/fish

pandoc -f markdown+smart metadata.yaml dejepis_seminarka.md --toc -V geometry:margin=1in -o dejepis_seminarka.pdf

